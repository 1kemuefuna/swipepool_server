var express = require('express');
var router = express.Router();
var paymentController = require('./paymentController.js');
var authMiddleware = require('../middleware');

/*
 * GET
 */
router.get('/', paymentController.list);

/*
 * GET
 */
router.get('/:id', paymentController.show);

/*
 * POST
 */
router.post('/', paymentController.create);

/*
 * PUT
 */ 
router.put('/:id', paymentController.update);

/*
 * DELETE
 */
router.delete('/:id', paymentController.remove);

router.post('/confirm', authMiddleware.ensureAuthenticated, paymentController.confirm);

module.exports = router;
