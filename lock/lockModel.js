var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var lockSchema = new Schema({
	'user_id' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'user'
	},
	'lig' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'lig'
	},
	'pattern' : String,
	'moves' : Number,
	'points' : Number,
	'score_array' : String,
	'pre_play_range': String,
	'pre_play_moves': Number,
	'date' : Date
});
 
module.exports = mongoose.model('lock', lockSchema);
