var express = require('express');
var router = express.Router();
var lockController = require('./lockController.js');
var authMiddleware = require('../middleware');
/*
 * GET
 */
router.get('/', lockController.list);

/*
 * GET
 */
router.get('/:id', lockController.show);
router.get('/show/:id', lockController.show_lock);

/*
 * POST
 */
router.post('/', authMiddleware.ensureAuthenticated, lockController.create); 

router.post('/process-lock', authMiddleware.ensureAuthenticated, lockController.process_lock);

router.post('/save-lock', authMiddleware.ensureAuthenticated, lockController.save_lock);

router.get('/get-lock/:id', authMiddleware.ensureAuthenticated, lockController.get_lock); 

/*
 * PUT
 */
router.put('/:id', lockController.update);

/*
 * DELETE
 */
router.delete('/:id', lockController.remove);

module.exports = router;
