var lockModel = require('./lockModel.js');
var ligModel = require('../lig/ligModel.js');
var userModel = require('../user/userModel.js');
var lockLogModel = require('../lock_log/lock_logModel.js');
var moment = require('moment');

/**
 * lockController.js
 *
 * @description :: Server-side logic for managing locks.
 */
module.exports = {

    /**
     * lockController.list()
     */
    list: function (req, res) {
        lockModel.find(function (err, locks) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting lock.',
                    error: err
                });
            }
            return res.json(locks);
        });
    }, 

    /**
     * lockController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        lockModel.findOne({_id: id}, function (err, lock) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting lock.',
                    error: err
                });
            }
            if (!lock) {
                return res.status(404).json({
                    message: 'No such lock'
                });
            }
            return res.json(lock);
        });
    },
    /**
     * lockController.show()
     */
    show_lock: function (req, res) {
        var id = req.params.id;
        lockModel.findOne({_id: id},).populate({path: 'lig', populate: {path: 'lock_logs', populate:{path: 'user'}}}).exec(function (err, lock) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting lock.',
                    error: err
                });
            }
            if (!lock) {
                return res.status(404).json({
                    message: 'No such lock'
                });
            }
            return res.json(lock);
        });
    },
    /**
     * lockController.show()
     */
    get_lock: function (req, res) {
        var id = req.params.id;
        lockModel.findOne({_id: id}).populate({path: 'lig', populate: {path: 'locks'}}).exec(function (err, lock) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting lock.',
                    error: err
                });
            }
            if (!lock) {
                return res.status(404).json({
                    message: 'No such lock'
                });
            }
            return res.json(lock);
        });
    },

    /**
     * lockController.create()
     */
    create: function (req, res) {
        var lock = new lockModel({
			user_id : req.user,
			lig : req.body.lig_id,
			pattern : req.body.pattern,
			moves : req.body.moves,
			points : req.body.points,
			score_array : req.body.score_array,
			date : req.body.date

        }); 

        lock.save(async function (err, lock) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating lock',
                    error: err
                });
            }

            let lig = await ligModel.findOne({_id: req.body.lig_id}).exec((err, lig)=>{
                lig.locks.push(lock._id);
                lig.save();
            }); 

          return res.status(201).json(lock);
        });
    },

    process_lock: function (req, res) {
        var id = req.body.lock_id;
        lockModel.findOne({_id: id}, function (err, lock) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting lock',
                    error: err
                });
            }
            if (!lock) {
                return res.status(404).json({
                    message: 'No such lock'
                });
            }
            
            lock.lig = req.body.lig_id,
            lock.pattern = req.body.pattern,
            lock.moves = req.body.moves,
            lock.points = req.body.points,
            lock.score_array = req.body.score_array,
            
            lock.save(async function (err, lock) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating lock.',
                        error: err
                    });
                }

                let lig = await ligModel.findOne({_id: req.body.lig_id}).exec((err, lig)=>{
                    lig.locks.push(lock._id);
                    lig.save();
                }); 

                //add virtual lock logs
                let locklog = await lockLogModel.findOne({user: req.user, lig: req.body.lig_id}).exec((err, locklogDoc)=>{
                        
                    if(locklogDoc){
                        lockLogModel.findOneAndUpdate({ _id: locklogDoc._id }, { $inc: { points: req.body.points } }, {new: true }).exec();
                    }else{
                        newLockLog = new lockLogModel({
                            'user_id' : req.user,
                            'user' : req.user,
                           'lig' : req.body.lig_id,
                           'points' : req.body.points,
                           'last_play' : moment().utc()
                        });
                        newLockLog.save()
                    }

                });

                return res.json(lock);
            });
        });
    },
 
    save_lock: function(req, res){
        var lock = new lockModel({
			user_id : req.user,
            lig : req.body.lig_id,
            pre_play_range: req.body.pre_play_range,
	        pre_play_moves: req.body.pre_play_moves,
            date : moment().unix()
        }); 
        lock.save(async function (err, lock) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating lock',
                    error: err
                });
            }

            let deduct = parseInt(req.body.pre_play_moves);
            userModel.updateOne({_id: req.user},{$inc:{moves:-deduct}}).exec();

            return res.status(201).json(lock);
        });

    },

    /**
     * lockController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        lockModel.findOne({_id: id}, function (err, lock) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting lock',
                    error: err
                });
            }
            if (!lock) {
                return res.status(404).json({
                    message: 'No such lock'
                });
            }

            lock.user_id = req.body.user_id ? req.body.user_id : lock.user_id;
			lock.lig_id = req.body.lig_id ? req.body.lig_id : lock.lig_id;
			lock.pattern = req.body.pattern ? req.body.pattern : lock.pattern;
			lock.moves = req.body.moves ? req.body.moves : lock.moves;
			lock.points = req.body.points ? req.body.points : lock.points;
			lock.score_array = req.body.score_array ? req.body.score_array : lock.score_array;
			lock.date = req.body.date ? req.body.date : lock.date;
			
            lock.save(function (err, lock) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating lock.',
                        error: err
                    });
                }

                return res.json(lock);
            });
        });
    },

    /**
     * lockController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        lockModel.findByIdAndRemove(id, function (err, lock) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the lock.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
