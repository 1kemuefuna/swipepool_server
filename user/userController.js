var userModel = require('./userModel.js');
var config = require('../config');
var request = require('request');
var jwt = require('jwt-simple');
var app_jwt = require('./jwt');

/**
 * userController.js
 *
 * @description :: Server-side logic for managing users.
 */
module.exports = {

    /**
     * userController.list()
     */
    list: function (req, res) {
        userModel.find(function (err, users) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting user.',
                    error: err
                });
            }
            return res.json(users);
        });
    },

    /**
     * userController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        userModel.findOne({_id: id}, function (err, user) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting user.',
                    error: err
                });
            }
            if (!user) {
                return res.status(404).json({
                    message: 'No such user'
                });
            }
            return res.json(user);
        });
    },

    /**
     * userController.show()
     */
    fetch_user: function (req, res, next) {
        var id = req.user;
        userModel.findOne({_id: id}, function (err, user) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting user.',
                    error: err
                });
            }
            if (!user) {
                return res.status(404).json({
                    message: 'No such user'
                });
            }
            return res.status(200).json(user);
        });
    },

    /**
     * userController.create()
     */
    create: function (req, res) {
        var user = new userModel({
			email : req.body.email,
			password : req.body.password,
			displayName : req.body.displayName,
			picture : req.body.picture,
			facebook : req.body.facebook,
			balance : req.body.balance,
			transfer_code : req.body.transfer_code,
			bvn : req.body.bvn,
			account_number : req.body.account_number,
			bank : req.body.bank,
			phone_number : req.body.phone_number,
			is_verified : req.body.is_verified

        });

        user.save(function (err, user) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating user',
                    error: err
                });
            }
            return res.status(201).json(user);
        });
    },

    /**
     * userController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        userModel.findOne({_id: id}, function (err, user) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting user',
                    error: err
                });
            }
            if (!user) {
                return res.status(404).json({
                    message: 'No such user'
                });
            }

            user.email = req.body.email ? req.body.email : user.email;
			user.password = req.body.password ? req.body.password : user.password;
			user.displayName = req.body.displayName ? req.body.displayName : user.displayName;
			user.picture = req.body.picture ? req.body.picture : user.picture;
			user.facebook = req.body.facebook ? req.body.facebook : user.facebook;
			user.balance = req.body.balance ? req.body.balance : user.balance;
			user.transfer_code = req.body.transfer_code ? req.body.transfer_code : user.transfer_code;
			user.bvn = req.body.bvn ? req.body.bvn : user.bvn;
			user.account_number = req.body.account_number ? req.body.account_number : user.account_number;
			user.bank = req.body.bank ? req.body.bank : user.bank;
			user.phone_number = req.body.phone_number ? req.body.phone_number : user.phone_number;
			user.is_verified = req.body.is_verified ? req.body.is_verified : user.is_verified;
			
            user.save(function (err, user) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating user.',
                        error: err
                    });
                }

                return res.json(user);
            });
        });
    },

    /**
     * userController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        userModel.findByIdAndRemove(id, function (err, user) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the user.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    },

    facebook: async function(req, res, next){
        var fields = ['id', 'email', 'first_name', 'last_name', 'link', 'name'];
        
        var accessTokenUrl = 'https://graph.facebook.com/v2.5/oauth/access_token';
        var graphApiUrl = 'https://graph.facebook.com/v2.5/me?fields=' + fields.join(',');
        var params = {
            code: req.body.oauthData.code,
            client_id: req.body.authorizationData.client_id,
            client_secret: config.FACEBOOK_SECRET,
            redirect_uri: req.body.authorizationData.redirect_uri
        };

        request.get({ url: accessTokenUrl, qs: params, json: true }, function(err, response, accessToken) {
            if (response.statusCode !== 200) {
              return res.status(500).send({ message: accessToken.error.message });
            }
        
            // Step 2. Retrieve profile information about the current user.
            request.get({ url: graphApiUrl, qs: accessToken, json: true }, function(err, response, profile) {
              if (response.statusCode !== 200) {
                return res.status(500).send({ message: profile.error.message });
              }
              if (req.header('Authorization')) {
                userModel.findOne({ facebook: profile.id }, function(err, existingUser) {
                  if (existingUser) {
                    return res.status(409).send({ message: 'There is already a Facebook account that belongs to you' });
                  }
                  var token = req.header('Authorization').split(' ')[1];
                  var payload = jwt.decode(token, config.TOKEN_SECRET);
                  userModel.findById(payload.sub, function(err, user) {
                    if (!user) {
                      return res.status(400).send({ message: 'User not found' });
                    }
                    user.facebook = profile.id;
                    user.picture = user.picture || 'https://graph.facebook.com/v2.3/' + profile.id + '/picture?type=large';
                    user.displayName = user.displayName || profile.name;
                    user.email = user.email;
                    user.save(function() {
                      var token = app_jwt.createJWT(user);
                      res.send({ token: token });
                    });
                  }); 
                });
              } else {
                // Step 3. Create a new user account or return an existing one.
                userModel.findOne({ facebook: profile.id }, function(err, existingUser) {
                  if (existingUser) {
                    var token = app_jwt.createJWT(existingUser);
                    return res.send({ token: token });
                  }
                  var user = new userModel();
                  user.facebook = profile.id;
                  user.picture = 'https://graph.facebook.com/' + profile.id + '/picture?type=large';
                  user.displayName = profile.name;
                  user.email = profile.email;
                  user.save(function() {
                    var token = app_jwt.createJWT(user);
                    res.send({ token: token });
                  });
                });
              }
            });
          });

    }
        
};
