var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var lock_logSchema = new Schema({
	'user_id' : String,
	'user' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'user'
	},
	'lig' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'lig'
	},
	'points' : Number,
	'last_play' : Date
});

module.exports = mongoose.model('lock_log', lock_logSchema);
