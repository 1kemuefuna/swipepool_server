var withdrawModel = require('./withdrawModel.js');

/**
 * withdrawController.js
 *
 * @description :: Server-side logic for managing withdraws.
 */
module.exports = {

    /**
     * withdrawController.list()
     */
    list: function (req, res) {
        withdrawModel.find(function (err, withdraws) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting withdraw.',
                    error: err
                });
            }
            return res.json(withdraws);
        });
    },

    /**
     * withdrawController.list()
     */
    get_accounts: function (req, res) {
        withdrawModel.find({user_id: req.user}, function (err, withdraws) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting withdraw.',
                    error: err
                });
            }
            return res.json(withdraws);
        });
    },

    /**
     * withdrawController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        withdrawModel.findOne({_id: id}, function (err, withdraw) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting withdraw.',
                    error: err
                });
            }
            if (!withdraw) {
                return res.status(404).json({
                    message: 'No such withdraw'
                });
            }
            return res.json(withdraw);
        });
    },

    /**
     * withdrawController.create()
     */
    create: function (req, res) {
        var withdraw = new withdrawModel({
			user_id : req.body.user_id,
			amount : req.body.amount,
			status : req.body.status,
			date : req.body.date

        });

        withdraw.save(function (err, withdraw) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating withdraw',
                    error: err
                });
            }
            return res.status(201).json(withdraw);
        });
    },

    /**
     * withdrawController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        withdrawModel.findOne({_id: id}, function (err, withdraw) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting withdraw',
                    error: err
                });
            }
            if (!withdraw) {
                return res.status(404).json({
                    message: 'No such withdraw'
                });
            }

            withdraw.user_id = req.body.user_id ? req.body.user_id : withdraw.user_id;
			withdraw.amount = req.body.amount ? req.body.amount : withdraw.amount;
			withdraw.status = req.body.status ? req.body.status : withdraw.status;
			withdraw.date = req.body.date ? req.body.date : withdraw.date;
			
            withdraw.save(function (err, withdraw) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating withdraw.',
                        error: err
                    });
                }

                return res.json(withdraw);
            });
        });
    },

    /**
     * withdrawController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        withdrawModel.findByIdAndRemove(id, function (err, withdraw) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the withdraw.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
