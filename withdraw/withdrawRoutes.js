var express = require('express');
var router = express.Router();
var withdrawController = require('./withdrawController.js');
var authMiddleware = require('../middleware');

/*
 * GET
 */
router.get('/', withdrawController.list);

/*
 * GET
 */
router.get('/:id', withdrawController.show);

/*
 * POST
 */
router.post('/', withdrawController.create);

/*
 * PUT
 */
router.put('/:id', withdrawController.update);

/*
 * DELETE
 */
router.delete('/:id', withdrawController.remove);

router.delete('/get-accounts', authMiddleware.ensureAuthenticated ,withdrawController.get_accounts);

module.exports = router;
